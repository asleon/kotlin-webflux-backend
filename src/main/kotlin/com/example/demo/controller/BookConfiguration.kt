package com.example.demo.controller

import com.example.demo.entity.Book
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.reactor.flux
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.reactive.function.server.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.stream.Stream

@Configuration
class BookConfiguration {

//    @GetMapping("/index")
//    fun getIndexPage(@RequestParam(required = false) text: String?):Mono<String> {
//        val result = if(!text.isNullOrBlank()) text else "Hello index controller"
//        return Mono.just(result)
//    }
//
//    @GetMapping("/books")
//    fun getBooks():Flux<Book> = Flux.fromStream(Stream.of<Book>(Book(1,"test"), Book(2,"varang")))

    val bookHandler = BookHandler()

    @Bean
    @FlowPreview
    fun route() = coRouter { accept(MediaType.APPLICATION_JSON)
            .nest {
                GET("/index", bookHandler::indexText)
                GET("/books", bookHandler::listBooks)
            }
    }

    class BookHandler{

        @FlowPreview
        suspend fun listBooks(request: ServerRequest): ServerResponse = ServerResponse
                    .ok()
                    .json()
                    .bodyAndAwait(Flux
                            .fromStream(Stream.of<Book>(Book(1,"test"),
                                            Book(2,"varang")))
                            .asFlow())

        suspend fun indexText(request: ServerRequest): ServerResponse {
            val text = request.queryParam("text")
            val result = if(!text.isEmpty) text.get() else "Hello index controller"
            return ServerResponse.ok().json().bodyAndAwait(Mono.just(result).asFlow())
        }
    }
}